function pr_stimulus(trial)

global exp

%% Draw the stimulus
clearpict(1);
% ver = A or B, then line
if exp.stim_ver == 'A' || exp.stim_ver == 'B'
    make_line;
end
% ver C or D, then patch
if exp.stim_ver == 'C' || exp.stim_ver == 'D'
    [~, ~, G1] = gabor('theta', exp.theta, 'lambda', 15, 'sigma', exp.scale/8, ... 
                      'width', 600, 'height', 600, 'px', 0.5, 'py', 0.5);
    G1 = G1 - min(min(G1));
    G1 = G1 / max(max(G1));
    clearpict(1);
    preparepict(G1, exp.buffer.stimulus);
end
wait(exp.times.stimulus);
drawpict(exp.buffer.stimulus)
exp.t = drawpict(exp.buffer.stimulus);
[exp.key, exp.keytime, exp.n] = waitkeydown(exp.times.cue);                 % Wait for response and get identity and time of pressed key


end

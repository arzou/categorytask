data = [exp.ARROWdata.ACC', exp.ARROWdata.RT'/1000, ...
        exp.ARROWdata.mean_rotation', exp.ARROWdata.dev_rotation', ...
        exp.ARROWdata.oddball_arrow'];
    
acc = zeros(5, 2);
    
row = 1;
for degrees = 4:2:12
    deg_trials = data(data(:,4) == degrees);
    neg_deg_trials = data(data(:,4) == -degrees);
    acc(row, 2) = nanmean([deg_trials; neg_deg_trials]);
    acc(row, 1) = degrees;
    row = row + 1;
end

save('Results/Jakob_data', 'data')
save('Results/Jakob_acc', 'acc')
function stim_list = get_category_trials(~)

raw_stim_list = csvread('category_trials');
ind = randperm(160);
stim_list = raw_stim_list(ind,:);

end

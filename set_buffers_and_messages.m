function set_buffers_and_messages

global exp

%% Clear all buffers
for buffer = 1:7
    clearpict(buffer)
end

exp.correctmsg = 'Good!';
exp.falsemsg = 'False!';

%% Name buffers
exp.buffer.stimulus                     = 1;
exp.buffer.fixation                     = 2;
exp.buffer.correct_response             = 3;
exp.buffer.blank                        = 4;
exp.buffer.false_response               = 5;
exp.buffer.wrong_key                    = 6;
exp.buffer.no_response                  = 7;


%% Load feedback into feedback buffers 
preparestring('Wrong key! (please use the number keys)', exp.buffer.wrong_key);
preparestring('No response!', exp.buffer.no_response);

%% Load fixation cross into buffer
preparestring('+', exp.buffer.fixation);

end
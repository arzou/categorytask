function category_task(category_version, stimulus_version)

global exp;

exp.numb_of_trials.category = 10; % temp
exp.stim_ver = stimulus_version; % A,B = line, C,D = gabor patch
exp.stim_list = get_category_trials;
exp.stimulus = exp.stim_list(1,:); % since list is already randomized you can start with the first elem
exp.points = 0; % counter for points
exp.task_ver = category_version;

%% Run the task
%get_category_ver
init_category
set_buffers_and_messages

%%% Give task instructions
pr_instruction;

for trial = 1:exp.numb_of_trials.category  % exp.numb_of_trials.categrory = number of trials for rotation task
    init_trial(trial); % Clear buffers, prepare buffers, and initialize truth values
    
    exp.stimulus = exp.stim_list(trial,:);
    exp.theta = exp.stimulus(1); % raw theta used for coordinates
    exp.scale = pi*20 + exp.stimulus(3)*40; % total length of stimuli

    exp.x1 = cos(exp.theta)*exp.scale;
    exp.y1 = sin(exp.theta)*exp.scale;
    exp.x2 = -exp.x1;
    exp.y2 = -exp.y1;
    
    if strcmp(exp.task_ver,'rb')
        exp.cat = exp.stimulus(4);
    elseif strcmp(exp.task_ver,'ii')
        exp.cat = exp.stimulus(5);
    end
    
    %%% task trial
    pr_stimulus(trial);
    exp.ACC = pr_feedback(trial);
    
    rec_trial(trial);   % Record trial information
    
    %%% Insert summary feedback
    if mod(trial,5) == 0
        pr_summary_feedback(trial);
        exp.points = 0;
    end
end

%% Say goodbye
pr_goodbye;                                                                 % Present goodbye slide
stop_cogent;

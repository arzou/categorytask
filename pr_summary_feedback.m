function pr_summary_feedback(trial)

global exp

%% Write and display the message
m1 = ['You got ', num2str(exp.points), ' of the last ', num2str(exp.summary_feedback_trial.category), ' trials right!'];
m2 = 'Press space to resume the task.';

cgpencol(1,1,1);
cgflip(0,0,0);
cgtext(m1, 0,  200); 
cgtext(m2, 0, -200); 
cgflip(0,0,0);
waitkeydown(inf, 71);

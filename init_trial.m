function init_trial(trial)

global exp;

%% Clear necessary buffers and keys
clearkeys;

%% Initialize truth values
exp.ACC                 = nan;
exp.key                 = [];
exp.keytime             = [];
exp.n                   = 0;
exp.catch_ACC           = nan;

end

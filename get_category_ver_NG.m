function get_category_ver

%% makes prompt boxes

global exp;

exp.data.language = 'English';
exp.STRATEGYdata = cell(10, 3);
exp.STRATEGYdata_counter = 1;

%% First window (task version)
fields = {'Task version: '};   % input is either 'rb' or 'ii'
subjectInfoEntryTitle = 'Subject Info Entry';
num_lines = 1;
answer = inputdlg(fields, subjectInfoEntryTitle, num_lines);
exp.task_ver      = (answer{1});


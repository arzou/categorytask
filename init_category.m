function init_category

global exp; % declares experiment variable

exp.number_of_summary_feedbacks = 2; % temp

%% Configure Cogent

% Configure display & keyboard - window size 0 for troubleshooting
config_display(0, 3, [0,0,0], [1 1 1], 'Helvetica', 40, 10, 0);   % Configure display (0 = window mode; 5 = 1280x1024; [1 1 1] = white background; grey text; fontname; fontsize; nbuffers)
config_keyboard;    % sets up background

%% Set Variables
exp.wdir                = cd;                                                       % Working directory

% Fix durations for everything in the experiment
exp.times.stimulus      =   500;                                                % Duration of stimulus presentation
exp.times.cue           = 25000;                                                    % Duration of key press cue
exp.times.feedback      =   500;                                                    % Duration of feedback 
exp.times.iti           = ones(1, exp.numb_of_trials.category);                     % Inter-trial interval [0.3 0.7] with mean 0.5
while mean(exp.times.iti) > .501 || mean(exp.times.iti) < .499
    exp.times.iti = 0.3 + 0.4 * rand(1, exp.numb_of_trials.category);
end
% Initialize log stuff
exp.CATdata.ACC             = zeros(1, exp.numb_of_trials.category);              % idk what this is
exp.CATdata.RT              = [];                                                 % reaction time
exp.CATdata.stime           = [];                                                 % Initialize (stimulus presentation time?)
exp.CATdata.key             = [];                                                 % Initialize for data saving
exp.CATdata.keytime         = [];                                                 % honestly probably odn't have to worry about this

% Set keys to select arrows and stimuli
exp.nkey.one          = 28;     % key index 2 corresonds to '1' 
exp.nkey.two          = 29;     % key index 3 corresponds to '2'
exp.nkey.three        = 30;
exp.nkey.four         = 31;

%% feedback trials
exp.summary_feedback_trial.category = round(exp.numb_of_trials.category / exp.number_of_summary_feedbacks);

    
%% Try to start cogent (else, wait for enter - to get matlab window)
try
    start_cogent;
catch
    input('Please press enter to continue')
    start_cogent;
end

end


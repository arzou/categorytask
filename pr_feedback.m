function ACC = pr_feedback(trial) 
global exp

%% get task version
if strcmp(exp.task_ver,'rb')
    exp.cat = exp.stimulus(4);
elseif strcmp(exp.task_ver,'ii')
    exp.cat = exp.stimulus(5);
end

%% Determine correct answer
if exp.cat == 1
    cor_key = exp.nkey.one;
    fal_key = [exp.nkey.two, exp.nkey.three, exp.nkey.four];
elseif exp.cat == 2
    cor_key = exp.nkey.two;
    fal_key = [exp.nkey.one, exp.nkey.three, exp.nkey.four];
elseif exp.cat == 3
    cor_key = exp.nkey.three;
    fal_key = [exp.nkey.one, exp.nkey.two, exp.nkey.four];
elseif exp.cat == 4
    cor_key = exp.nkey.four;
    fal_key = [exp.nkey.one, exp.nkey.two, exp.nkey.three];
end

%% Determine accuracy and present feedback
if ~isempty(exp.key)
    if exp.key == cor_key
        clearpict(3);
        ACC = 1;
        exp.points = exp.points + 1;
        exp.correctmsg = ['Good! The category was ',num2str(exp.cat),'.'];
        preparestring(exp.correctmsg, exp.buffer.correct_response);
        drawpict(exp.buffer.correct_response);
        waitkeydown(exp.times.feedback)
    elseif (exp.key ~= cor_key && any(fal_key==exp.key))
        clearpict(5);
        ACC = 0;
        exp.falsemsg = ['False! The category was ',num2str(exp.cat),'.'];
        preparestring(exp.falsemsg, exp.buffer.false_response);
        drawpict(exp.buffer.false_response);
        waitkeydown(2 * exp.times.feedback)
    else
       ACC = nan;
       drawpict(exp.buffer.wrong_key)
       waitkeydown(2 * exp.times.feedback)
    end
else
    ACC = nan;
    drawpict(exp.buffer.no_response)
    waitkeydown(2 * exp.times.feedback)
end

%% Wait ITI
drawpict(exp.buffer.fixation);                                                                % Fixation screen
wait(exp.times.iti(trial));

end

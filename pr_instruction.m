function pr_instruction

%start_cogent;

global exp;

upper_end = 300;

%% Write messages
    m11 = 'In the following task, you learn about 4 categories of lines';
    m12 = 'labeled categories 1, 2, 3, and 4,';
    m13 = 'and assign the lines you see to one of these categories.';
    m21 = 'To choose a category, you will press';
    m22 = 'the corresponding key on the keyboard.';
    m23 = 'After you choose a category, we will tell you';
    m24 = 'if that was the right category for that line.';
    m31 = 'Please use this feedback to learn about the categories';
    m32 = 'and help you make your decisions about the lines you see later,';
    m33 = 'so that you make fewer errors as you progress through this task.';
    m41 = 'Any questions?';
    m42 = 'Press the space key when you want to start.';

% slide 1
clearkeys;
clearpict(1)    % refreshes the display buffer for instructions
preparestring(m11, 1, 0, 90);
preparestring(m12, 1, 0, 20);
preparestring(m13, 1, 0, -50);
drawpict(1);
waitkeydown(inf, 71);

% slide 2
clearpict(1);
preparestring(m21, 1, 0, 90);
preparestring(m22, 1, 0, 20);
preparestring(m23, 1, 0, -50);
preparestring(m24, 1, 0, -120);
drawpict(1);
waitkeydown(inf, 71);

% slide 3
clearpict(1);
preparestring(m31, 1, 0, 90);
preparestring(m32, 1, 0, 20);
preparestring(m33, 1, 0, -50);
drawpict(1);
waitkeydown(inf, 71)

% last slide
clearpict(1);
preparestring(m41, 1, 0, 90);
preparestring(m42, 1, 0, 20);
drawpict(1);
waitkeydown(inf, 71)

end
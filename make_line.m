function make_line

global exp;

%% Make the line using theta and scale the length

% exp.theta = exp.stimulus(1); % raw theta used for coordinates
% exp.scale = pi*20 + exp.stimulus(3)*40; % total length of stimuli
% 
% exp.x1 = cos(exp.theta)*exp.scale;
% exp.y1 = sin(exp.theta)*exp.scale;
% exp.x2 = -exp.x1;
% exp.y2 = -exp.y1;

%% Getting category type
% if strcmp(exp.task_ver,'rb')
%      exp.cat = exp.stimulus(4);
% elseif strcmp(exp.task_ver,'ii')
%      exp.cat = exp.stimulus(5);
% end

% troubleshooting - uncomment as needed
% if exp.cat == 1
%     cgpencol(1,1,1);
% elseif exp.cat == 2
%     cgpencol(1,0,0);
% elseif exp.cat == 3
%     cgpencol(0,1,0);
% elseif exp.cat == 4
%     cgpencol(0,0,1);
% end

cgpencol(1,1,1);
cgpenwid(2);
cgdraw(exp.x1,exp.y1,exp.x2,exp.y2)
cgflip(0,0,0);

end
function pr_goodbye

global exp;

m1 = 'Thank you!';
m2 = 'You have done a great job!';
m3 = 'The next task will start soon.';

clearpict(5);

preparestring(m1, 5, 0,  100);
preparestring(m2, 5, 0,   50);
preparestring(m3, 5, 0, -100);

drawpict(5);
wait(3000);